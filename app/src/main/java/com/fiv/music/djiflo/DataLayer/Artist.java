package com.fiv.music.djiflo.DataLayer;

/**
 * Created by apple on 06/09/2017.
 */

public class Artist {
    private String Name,ImageURL,Date;

    public Artist() {
    }

    public Artist(String name, String imageURL) {
        Name = name;
        ImageURL = imageURL;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
