package com.fiv.music.djiflo.DataLayer;

/**
 * Created by apple on 06/09/2017.
 */

public class Category {
    private String Name;

    public Category(String name) {
        Name = name;
    }

    public Category() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
