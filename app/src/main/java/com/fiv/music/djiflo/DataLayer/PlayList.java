package com.fiv.music.djiflo.DataLayer;

import java.io.Serializable;

/**
 * Created by apple on 19/11/2017.
 */

public class PlayList implements Serializable {
    private String Name;
    private String ImageURL;
    private String Id;
    private String Date;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        this.Date = date;
    }
}
