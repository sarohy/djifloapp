package com.fiv.music.djiflo.DataLayer;

import java.util.ArrayList;

/**
 * Created by apple on 05/09/2017.
 */

public class Song {
    private String ArtURL,Artist,Category,Duration,Name,MusicURL,Stream,Date;
    private String MusicId;

    public Song() {
    }

    public Song(String ArtURL, String Artist, String Category, String Duration, String Name, String MusicURL, String Stream) {
        this.ArtURL = ArtURL;
        this.Artist = Artist;
        this.Category = Category;
        this.Duration = Duration;
        this.Name = Name;
        this.MusicURL = MusicURL;
        this.Stream = Stream;
    }

    public String getArtURL() {
        return ArtURL;
    }

    public void setArtURL(String artURl) {
        ArtURL = artURl;
    }

    public String getArtist() {
        return Artist;
    }

    public void setArtist(String artist) {
        Artist = artist;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMusicURL() {
        return MusicURL;
    }

    public void setMusicURL(String musicURL) {
        MusicURL = musicURL;
    }

    public String getStream() {
        return Stream;
    }

    public void setStream(String stream) {
        Stream = stream;
    }

    public String getMusicId() {
        return MusicId;
    }

    public void setMusicId(String musicId) {
        MusicId = musicId;
    }


    public void incStream() {
        int s= Integer.parseInt(Stream);
        s++;
        Stream= String.valueOf(s);
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        this.Date = date;
    }
}
